# Dokumentation von *BasketTurret*

## Inhalt

1. anfängliche Vorstellungen
2. Dokumentation der Stunden
3. Erläuterung des Quelltexts 
4. Inspirationen
5. Quellen

## Anfängliche Vorstellungen

## Dokumentation der Stunden

### Stunde 1: 28.10.2014

In der ersten Stunde haben sich Tobias und Paul um das Fahrwerk des Roboters gekümmert. Dabei ist ein einfaches, nicht ganz festes Gestell zusammen gekommen.

[Fahrwerk](Lesson_01_Fahrwerk.jpg)

Währenddessen hat Jannik ein sehr überproportionales Schießwerk aufgebaut. Dieses bestand aus einem dauerhaft gespanntem Gummiband mit einem Schlitten, der ein Lego-Teilchen bewegt. Dieses war von einem Haken gestoppt, welcher per Motor weggehen sollte und das Lego-Teilchen den Weg zum Schuss freigab.

**Problem:** Das Gerüst stand ständig unter Spannung, war zu den Seiten nahezu ungesichert und nicht sehr stabil. Schon in der nächsten Stunde haben wir diese Konstruktion verworfen.

[Geschoss](Stunde_01_Geschoss.jpg)

### Stunde 2: 11.10.2014

In der Stunde haben wir uns mit der Stabilität des fahrbaren Teils des Roboters auseinandergesetzt. Dabei wurden Querstreben und Verankerungen eingesetzt. Inzwischen sieht das Fahrwerk so aus:

[Fahrwerk](Stunde_02_Fahrwerk.jpg)

## Erläuterung des Quelltexts

## Inspiration

## Quellen